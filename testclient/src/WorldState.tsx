import create from 'zustand'

export type Entity = {
    id: number;
    components: {
        movement: {
            x: number;
            y: number;
        },
        position: {
            x: number;
            y: number;
        }
    };
};

export type Entities = {
    [key: string]: Entity;
};

export type WorldState = {
    entities: Entities;
    updateEntities: (data : WorldState) => void;
};

const WorldStateStore = create<WorldState>(
    (set) => {

        const updateEntities = (data : WorldState) => {
            set({
                entities: data.entities
            })
        }

        return {
            entities: {},
            updateEntities: updateEntities,
        }
    }
)

export default WorldStateStore;