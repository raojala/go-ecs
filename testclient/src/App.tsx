import React from 'react';
import { Map } from './Canvas';
import './main.css';

function App() {
  return (
    <>
      <Map />
    </>
  );
}

export default App;
