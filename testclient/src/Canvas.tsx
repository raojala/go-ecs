import React, { useEffect, useRef } from 'react';
import WorldStateStore, { Entities, Entity } from './WorldState';

export const Map = () => {
    const canvasRef = useRef<HTMLCanvasElement>(null);

    const entities = WorldStateStore((state) => state.entities)

    useEffect(() => {
        const ctx = canvasRef.current?.getContext('2d') as CanvasRenderingContext2D;
        DrawUpdate(ctx, entities);
    }, [entities]);

    return (
        <canvas ref={canvasRef} id="myCanvas"></canvas>
    )
}

const DrawUpdate = (ctx: CanvasRenderingContext2D, entities: Entities) => {

    const drawEntity = (ctx: CanvasRenderingContext2D, entity: Entity) => {
        ctx.fillStyle = '#00FF5E';
        ctx.strokeStyle = 'black';
    
        ctx.beginPath();
        ctx.arc(entity.components.position.x, entity.components.position.y, 5, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
    }

    const redrawBackground = () => {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.fillStyle = "#D308DA";
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    };

    const updateEntities = () => {
        for (let prop in entities) {
            drawEntity(ctx, entities[prop]);
        }
    }

    redrawBackground();
    updateEntities();
}