import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import WorldStateStore from './WorldState';

const socket = new WebSocket('ws://localhost:8080');
socket.addEventListener('open', (event: any) => {
  console.log('open', event)
});
socket.addEventListener('message', (event: any) => {
  WorldStateStore.getState().updateEntities(JSON.parse(event.data))
});
socket.addEventListener('error', (event: any) => {
  console.log('error', event)
});
socket.addEventListener('close', (event: any) => {
  console.log('close', event)
});

const rootElement = document.getElementById('root');
ReactDOM.render(<React.StrictMode><App /></React.StrictMode>, rootElement);