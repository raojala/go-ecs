package business

type Position struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type Movement struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}
