package business

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"

	"github.com/gorilla/websocket"
)

const (
	UPDATE_MOVEMENT_SYSTEM        = "updateMovement"
	UPDATE_POSITION_SYSTEM        = "updatePosition"
	UPDATE_NETWORK_BODIES_SYSTEM  = "updateNetworkBodies"
	CLEANUP_NETWORK_BODIES_SYSTEM = "cleanUpNetworkBodies"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func updateMovement(entities map[int]*Entity) {
	if len(entities) == 0 {
		return
	}

	for _, entity := range entities {
		moveComp := entity.GetComponent(Movement{}).(*Movement)
		moveComp.X = rand.Float64() * 100
		moveComp.Y = rand.Float64() * 100
	}
}

func updatePosition(entities map[int]*Entity) {
	if len(entities) == 0 {
		return
	}

	for _, entity := range entities {
		positionComp := entity.GetComponent(Position{}).(*Position)
		moveComp := entity.GetComponent(Movement{}).(*Movement)
		positionComp.X = moveComp.X
		positionComp.Y = moveComp.Y
	}
}

func updateNetworkBodies(world *World, entities map[int]*Entity) {
	if len(entities) == 0 {
		return
	}

	worldJSON, err := json.Marshal(world)
	if err != nil {
		fmt.Println(err)
		return
	}

	for _, entity := range entities {
		networkBody := entity.GetComponent(NetworkBody{}).(*NetworkBody)
		networkBody.SendMessage(worldJSON)
	}
}

func HandleConnections(world *World) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil) //websocket
		if err != nil {
			fmt.Println(err)
			return
		}

		// Create a new dot representing the connection
		player := NewPlayer(conn)
		world.AddEntity(player)

		networkBody := player.GetComponent(NetworkBody{}).(*NetworkBody)
		networkBody.Start(world, player)
	})
}

func cleanUpNetworkBodies(world *World, entities map[int]*Entity) {
	if len(entities) == 0 {
		return
	}

	for _, entity := range entities {
		networkBody := entity.GetComponent(NetworkBody{}).(*NetworkBody)
		networkBody.Close()
	}
}
