package business

import (
	"github.com/gorilla/websocket"
)

func NewPlayer(conn *websocket.Conn) *Entity {
	player := NewEntityHandle()
	player.AddComponent(&Position{})
	player.AddComponent(&Movement{})
	player.AddComponent(NewNetworkBodyComponent(conn))
	player.CleanUp = CleanUpPlayer
	return player
}

func CleanUpPlayer(player *Entity) {
	nb := player.GetComponent(NetworkBody{}).(*NetworkBody)
	nb.Close()
}
