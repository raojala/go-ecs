package business

import (
	"sync"
	"time"
)

type World struct {
	EntitiesLock sync.Mutex      `json:"-"`
	Entities     map[int]*Entity `json:"entities"`

	VariableTimeSystems map[string]func(map[int]*Entity) `json:"-"`
	FixedTimeSystems    map[string]func(map[int]*Entity) `json:"-"`
	CleanUpSystems      map[string]func(map[int]*Entity) `json:"-"`

	DeltaTime  float64   `json:"-"`
	GameTime   float64   `json:"-"`
	lastUpdate time.Time `json:"-"`
}

func InitWorld() *World {
	world := &World{
		EntitiesLock:        sync.Mutex{},
		Entities:            make(map[int]*Entity),
		VariableTimeSystems: make(map[string]func(map[int]*Entity)),
		FixedTimeSystems:    make(map[string]func(map[int]*Entity)),
		CleanUpSystems:      make(map[string]func(map[int]*Entity)),
		lastUpdate:          time.Now(),
	}
	world.VariableTimeSystems[UPDATE_MOVEMENT_SYSTEM] = updateMovement
	world.VariableTimeSystems[UPDATE_POSITION_SYSTEM] = updatePosition

	world.FixedTimeSystems[UPDATE_NETWORK_BODIES_SYSTEM] = func(entities map[int]*Entity) {
		updateNetworkBodies(world, entities)
	}

	world.CleanUpSystems[CLEANUP_NETWORK_BODIES_SYSTEM] = func(entities map[int]*Entity) {
		cleanUpNetworkBodies(world, entities)
	}

	return world
}

func (world *World) Update() {
	// Calculate the elapsed time since the last update
	world.DeltaTime = time.Since(world.lastUpdate).Seconds()
	world.lastUpdate = time.Now()

	// Update game time
	world.GameTime += world.DeltaTime

	for _, system := range world.VariableTimeSystems {
		world.EntitiesLock.Lock()
		system(world.Entities)
		world.EntitiesLock.Unlock()
	}
}

func (world *World) FixedUpdate() {
	for _, system := range world.FixedTimeSystems {
		world.EntitiesLock.Lock()
		system(world.Entities)
		world.EntitiesLock.Unlock()
	}
}

func (world *World) CleanUp() {
	for _, system := range world.CleanUpSystems {
		world.EntitiesLock.Lock()
		system(world.Entities)
		world.EntitiesLock.Unlock()
	}
}

func (world *World) AddEntity(entity *Entity) {
	world.EntitiesLock.Lock()
	world.Entities[entity.ID] = entity
	world.EntitiesLock.Unlock()
}

func (world *World) RemoveEntity(entityId int) {
	world.EntitiesLock.Lock()
	ent, ok := world.Entities[entityId]
	if !ok {
		// entity id not found, already deleted
		world.EntitiesLock.Unlock()
		return
	}
	ent.CleanUp(ent)
	delete(world.Entities, ent.ID)
	world.EntitiesLock.Unlock()
}
