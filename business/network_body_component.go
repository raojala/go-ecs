package business

import (
	"sync"

	"github.com/gorilla/websocket"
)

const (
	NETWORK_BODY_READ_BUFFER_SIZE  = 4096
	NETWORK_BODY_WRITE_BUFFER_SIZE = 4096
)

type NetworkBody struct {
	Connection *websocket.Conn `json:"-"`
	ReadMutex  sync.Mutex      `json:"-"`
	ReadQueue  chan []byte     `json:"-"`
	WriteMutex sync.Mutex      `json:"-"`
	WriteQueue chan []byte     `json:"-"`
}

func NewNetworkBodyComponent(conn *websocket.Conn) *NetworkBody {
	nb := &NetworkBody{
		Connection: conn,
		ReadQueue:  make(chan []byte, NETWORK_BODY_READ_BUFFER_SIZE),
		WriteQueue: make(chan []byte, NETWORK_BODY_WRITE_BUFFER_SIZE),
	}
	return nb
}

func (nb *NetworkBody) SendMessage(message []byte) {
	nb.WriteMutex.Lock()
	nb.WriteQueue <- message
	nb.WriteMutex.Unlock()
}

func (nb *NetworkBody) Start(world *World, entity *Entity) {
	go nb.ReadHandler(nb.ReadQueue, nb.Connection, world, entity)
	go nb.WriteHandler(nb.WriteQueue, nb.Connection, world, entity)
}

func (nb *NetworkBody) Close() {
	nb.Connection.Close()

	select {
	case _, ok := <-nb.ReadQueue:
		if ok {
			close(nb.ReadQueue)
		}
	default:
	}

	select {
	case _, ok := <-nb.WriteQueue:
		if ok {
			close(nb.WriteQueue)
		}
	default:
	}
}

func (nb *NetworkBody) ReadHandler(ReadQueue <-chan []byte, conn *websocket.Conn, world *World, entity *Entity) {

	for {
		/*message, ok*/ _, ok := <-ReadQueue
		// process message here!
		if !ok {
			world.RemoveEntity(entity.ID)
			break
		}
	}
}

func (nb *NetworkBody) WriteHandler(WriteQueue <-chan []byte, conn *websocket.Conn, world *World, entity *Entity) {

	for {
		message, ok := <-WriteQueue
		if !ok {
			world.RemoveEntity(entity.ID)
			break
		}
		err := conn.WriteMessage(websocket.TextMessage, message)
		if err != nil {
			world.RemoveEntity(entity.ID)
			break
		}
	}
}
