package business

import (
	"reflect"
	"strings"
	"sync"
)

type Entity struct {
	ID             int                    `json:"id"`
	ComponentsLock sync.Mutex             `json:"-"`
	Components     map[string]interface{} `json:"components"`
	CleanUp        func(*Entity)          `json:"-"`
}

var currentID int = 0

func NewEntityHandle() *Entity {
	ent := &Entity{
		ID:             currentID,
		ComponentsLock: sync.Mutex{},
		Components:     make(map[string]interface{}),
	}
	currentID++
	return ent
}

func (e *Entity) AddComponent(component interface{}) {
	e.ComponentsLock.Lock()
	e.Components[getComponentName(component)] = component
	e.ComponentsLock.Unlock()
}

func (e *Entity) GetComponent(component interface{}) interface{} {
	e.ComponentsLock.Lock()
	comp := e.Components[getComponentName(component)]
	e.ComponentsLock.Unlock()
	return comp
}

func getComponentName(component interface{}) string {
	if t := reflect.TypeOf(component); t.Kind() == reflect.Ptr {
		return strings.ToLower(t.Elem().Name())
	} else {
		return strings.ToLower(t.Name())
	}
}
