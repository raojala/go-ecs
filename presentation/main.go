package main

import (
	"business"
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var world *business.World
var ticker *time.Ticker

const (
	tickRate = 2
)

func main() {
	// Create a channel to receive the interrupt signal
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// initialize world
	world = business.InitWorld()
	if world == nil {
		panic(errors.New("world nil after setup"))
	}

	// Create a ticker for the fixed update rate
	ticker = time.NewTicker(time.Second / tickRate)

	// Start HTTP server to handle websocket connections
	srv := &http.Server{
		Addr:    "localhost:8080",
		Handler: business.HandleConnections(world),
	}
	go func() {
		srv.ListenAndServe()
	}()

	for {
		// variable time step
		update()

		select {
		case <-sig:
			// Check if the game loop should be interrupted
			fmt.Println("Game loop interrupted. Shutting down...")
			srv.Shutdown(context.Background())
			close()
			fmt.Println("Exiting...")
			return
		case <-ticker.C:
			// fixed time step for physics and network
			fixedUpdate()
		default:
		}
	}
}

func update() {
	// Update game state
	world.Update()
}

func fixedUpdate() {
	// Update game state
	world.FixedUpdate()
}

func close() {
	// clean up things that need to be disposed
	// such as running go routines and such
	ticker.Stop()
	world.CleanUp()
}
