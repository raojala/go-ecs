# go-ecs

this repo is an example of an Entity component system and Data driven development. These
concepts have been studied before, but this is a product of chatGPT assisted learning
and development.

ChatGPT logs can be found in the repo folder "chatGPT-logs"

ECS code is located in business folder and presentation is the main "game server"

The simulation is a simple multiclient socket application that runs updatePosition and
updateMovement systems.

simulation randomly places the clients dot on a 100x100 grid and informs the client of the
world state.

client draws the worldstate to canvas

## Requirements

Developed using:
- go version go1.19.2 windows/amd64
- node version 18.12.1
- npm version 8.19.2

## start up

1. "npm install" inside testclient folder
2. "npm start" inside testclient folder
3. localhost:3000 in browser
4. "go run .\presentation\" in terminal in root folder (one with go.work file)

every browser window on the localhost:3000 should be another updating dot on the
canvas. closing browser windows should remove dots after the socket times out.

1. To quit press ctrl+c in terminal