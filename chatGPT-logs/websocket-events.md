# what are possible events for websocket in typescript

In TypeScript, WebSockets provide a protocol for full-duplex communication channels over a single TCP connection. You can use WebSockets with the WebSocket API or with the higher-level ws library.

Here is a list of some of the possible events that you might encounter when working with WebSockets in TypeScript:

    open: This event is emitted when the WebSocket connection is established.

    message: This event is emitted when a message is received over the WebSocket connection. The message can be a string, a binary data buffer, or a data object.

    close: This event is emitted when the WebSocket connection is closed.

    error: This event is emitted when an error occurs with the WebSocket connection.

    ping: This event is emitted when a ping message is received over the WebSocket connection.

    pong: This event is emitted when a pong message is received over the WebSocket connection.

    upgrade: This event is emitted when the WebSocket connection is upgraded from an HTTP connection.

    upgradeError: This event is emitted when an error occurs during the upgrade process from an HTTP connection to a WebSocket connection.

    unexpected-response: This event is emitted when an unexpected response is received while trying to upgrade an HTTP connection to a WebSocket connection.

    connection: This event is emitted when a new WebSocket connection is established (when using the ws library).

    headers: This event is emitted when headers are received (when using the ws library).

    listening: This event is emitted when the WebSocket server is ready to accept connections (when using the ws library).

    connect: This event is emitted when a new WebSocket connection is established to a server (when using the ws library).

    disconnect: This event is emitted when a WebSocket connection is closed (when using the ws library).

    authentication: This event is emitted when an authentication request is received (when using the ws library).

I hope this helps! Let me know if you have any other questions.
